#!/bin/bash

API_VERSION=$(grep API_VERSION .env | cut -d '=' -f 2-)

realpath=$(realpath $0)
dirPath=$(dirname $realpath)

pathLayer=$dirPath/layers/$API_VERSION/nodejs

# Install all dependencies for this API's version
npm $1 --prefix $pathLayer $2 $3 $4 $5 $6 $7