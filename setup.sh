#!/bin/bash

npm install -g serverless@1.71.1 --force

API_VERSION=$(grep API_VERSION .env | cut -d '=' -f 2-)

realpath=$(realpath $0)
dirPath=$(dirname $realpath)

NODE_MODULES_ROOT=$dirPath/node_modules
NODE_MODULES_OF_CURRENT_API=$dirPath/layers/$API_VERSION/nodejs/node_modules
PATH_LAYER=$dirPath/layers/$API_VERSION/nodejs

# Install all dependencies for this API's version
npm install --prefix $PATH_LAYER

if test -f $NODE_MODULES_ROOT; then
    rm $NODE_MODULES_ROOT
fi

# Make a link to the node_modules of the current API's version.
ln -s $NODE_MODULES_OF_CURRENT_API $NODE_MODULES_ROOT

# Make sure all functions will be registerd with the cloudformation service.
serverless deploy

# Do a test function to make sure all configs is working correctly.
serverless invoke local -f test --path src/$API_VERSION/events/test.json
