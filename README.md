# README #

### Configs ###

Copy **.env.example** to **.env** and enter these configs

### Install source code ###

~~~~
sh setup.sh
~~~~

### Deploy source code ###

~~~~
sh deploy_apis.sh
~~~~

### Deploy layers when you added new package ###

~~~~
sh deploy_layers.sh
~~~~

Getting new ARN from that layer to fill into .env file. 

### Add new packages ###

~~~~
sh dependencies.sh <install|uninstall> <package@version> <any> <any> <any> <any>
~~~~
Ex: add new moment package
~~~~
sh dependencies.sh install moment@1.0.0 --save
~~~~
Ex: remove new moment package
~~~~
sh dependencies.sh uninstall moment@1.0.0 --save
~~~~
**Note:** You have to re-run **deploy_layers.sh** and update the **.env** file to update new packages to the project.
 
### Run the project ###

~~~~
serverless offline
~~~~

After you ran this command line, you don't need to stop and run again when you add new code to the project until you add new functions to serverless.yml.

The **serverless-offline** plugin will automatically update the new changes, you just do only one thing is refesh your browser.