#!/bin/bash

API_VERSION=$(grep API_VERSION .env | cut -d '=' -f 2-)

realpath=$(realpath $0)
dirPath=$(dirname $realpath)

NODE_MODULES_ROOT=$dirPath/node_modules
NODE_MODULES_OF_CURRENT_API=$dirPath/layers/$API_VERSION/nodejs/node_modules

# Remove link to make sure after creating a new link we are pointed to right folder.
if [ -e "$NODE_MODULES_ROOT" ]; then
    rm $NODE_MODULES_ROOT
fi

# Make a link to the node_modules of the current API's version.
ln -s $NODE_MODULES_OF_CURRENT_API $NODE_MODULES_ROOT

serverless deploy