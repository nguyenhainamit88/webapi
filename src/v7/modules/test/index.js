
const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send([
        "API_VERSION: " + process.env.API_VERSION,
        "REGION: " + process.env.REGION
    ]);
});

module.exports = router;