const winston = require('winston');
require('express-async-errors');

const logger = winston.createLogger({
    transports: [
        new winston.transports.Console({ colorize: true, prettyPrint:true })
    ],
    exceptionHandlers: [
        new winston.transports.Console()
    ]
});

module.exports = {
    logger,
    logging: function () {
        process.on('unhandledRejection', (ex) => {
            throw ex;
        });
        
        process.on('uncaughtException', (ex) => {
            logger.error(ex.message, ex);
        });
    }
}