const express = require('express');
const home = require('../modules/home/index');
const test = require('../modules/test/index');
const { error } = require('../middlewares/error');

module.exports = function (app) {
    app.use(express.json());
    app.use('/v7/home', home);
    app.use('/v7/test', test);
    app.use(error);
}