
'use strict';

const serverless = require('serverless-http');
const express = require('express')
const app = express()
const { logging } = require('./statups/logging');
require('dotenv').config();
require('./statups/routes')(app);

// Logging all of errors in system
logging();

module.exports.handler = serverless(app);