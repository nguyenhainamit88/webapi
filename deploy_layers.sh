#!/bin/bash

API_VERSION=$(grep API_VERSION .env | cut -d '=' -f 2-)

realpath=$(realpath $0)
dirPath=$(dirname $realpath)

cd $dirPath/layers/$API_VERSION

serverless deploy

cd $dirPath